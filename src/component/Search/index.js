import React from 'react';
import axios from 'axios'
import PropTypes from 'prop-types'

import { Container, Form, Item, Rating } from "semantic-ui-react"
import './style.scss';
import { Link } from 'react-router-dom';

class Search extends React.Component {
  constructor(props) {
    super(props);
    console.log(typeof(props.apikey));
    console.log(props.apikey);
    this.state = { movies: [], activeStandard: "title", activeOrder: "ascending" }; 

    // this.handleChange = this.handleChange.bind(this);
    this.api_endpoint = `https://api.themoviedb.org/3/movie/550?api_key=${props.apikey}`
    this.sortOptions = [
      { key: 1, text: "Title", value: "title" },
      { key: 2, text: "Rating", value: "rating" },
    ];
    this.orderOptions = [
      { key: 1, text: "Ascending", value: "ascending" },
      { key: 2, text: "Descending", value: "descending" },
    ]
  }

  // componentDidMount() {
  //   axios.get(this.api_endpoint)
  //       .then(res => {
  //         console.log(res)
  //         this.setState({ songs: res.data });
  //       })
  // }

  // componentDidUpdate(prevProps) {
  //   if (this.props.userID !== prevProps.userID) {
  //     this.setState((state, props) => { movies: this.movies.sort(function(a, b) { 
  //       if (this.activeStandard === "title") {
  //         return a.original_title.localeCompare(b.original_title);
  //       } else {
  //         return a.vote_average - b.vote_average;
  //       }
  //     })})
  //   }
  // }

  // handleClick = (e) => {

  // }

  onSearchChange = (e, { value }) => {
    console.log(value)
    if (value !== "") {
      let search_api = `https://api.themoviedb.org/3/search/movie?api_key=${this.props.apikey}&language=en-US&page=1&include_adult=false&query=${value}`
      axios.get(search_api)
      .then(res => {
        // this.setState({ movies: res.data.results })
        this.setState((state) => ({ movies: res.data.results.sort(function(a, b) { 
          if (state.activeStandard === "title") {
            // console.log(a.original_title)
            if (state.activeOrder === "ascending") {
              return a.original_title.localeCompare(b.original_title);
            } else {
              return b.original_title.localeCompare(a.original_title);
            }
          } else {
            if (state.activeOrder === "descending") {
              return a.vote_average - b.vote_average;
            } else {
              return b.vote_average - a.vote_average;
            }
          }
        })}))
      })
    }
  }

  onSortChange = (e, { value }) => {
    console.log(value);
    this.setState((state) => ({ activeStandard: value, movies: state.movies.sort(function(a, b) { 
      if (value === "title") {
        if (state.activeOrder === "ascending") {
          return a.original_title.localeCompare(b.original_title);
        } else {
          return b.original_title.localeCompare(a.original_title);
        }
      } else { 
        if (state.activeOrder === "descending") {
          return b.vote_average - a.vote_average;
        } else {
          return a.vote_average - b.vote_average;
        }
      }
    })}))
  }

  onOrderChange = (e, { value }) => {
    console.log(value);
    this.setState((state) => ({ activeOrder: value, movies: state.movies.sort(function(a, b) { 
      if (state.activeStandard === "title") {
        if (value === "ascending") {
          return a.original_title.localeCompare(b.original_title);
        } else {
          return b.original_title.localeCompare(a.original_title);
        }
      } else {
        if (value === "ascending") {
          return a.vote_average - b.vote_average;
        } else {
          return b.vote_average - a.vote_average;
        }
      }
    })}))
  }

  // handleChange(event) {
  //   const { onChange } = this.props;
  //   onChange(event);
  // }

  render() {
    // const { location } = this.props
    // let background = location.state && location.state.background;

    return (
      // <Router>

        <Container>
          <Form size='large' className="Form">
            <Form.Input fluid placeholder='Search...' onChange={this.onSearchChange} />
            <p>Sort By</p>
            <Form.Dropdown placeholder="Title" selection options={this.sortOptions} onChange={this.onSortChange} value={this.state.activeStandard} />
            <p>Order</p>
            <Form.Dropdown placeholder="Ascending" selection options={this.orderOptions} onChange={this.onOrderChange} value={this.state.activeOrder} />
          </Form>
          <Item.Group divided>
            {this.state.movies.map((movie) => (
                <Item key={movie.id} onClick={this.handleClick}
                as={ Link }
                to={`/detail/${movie.id}`}
                >
                  <Item.Image 
                    size="small" 
                    className="poster" 
                    src={ movie.poster_path !== null ? `https://image.tmdb.org/t/p/w500/${movie.poster_path}` : "https://critics.io/img/movies/poster-placeholder.png" } 
                  />
                  <Item.Content>
                    <Item.Header as='h2'>{movie.original_title}</Item.Header>
                    <Item.Extra>
                      <Rating icon='star' disabled defaultRating={1} maxRating={1}></Rating> 
                      {movie.vote_average}  
                    </Item.Extra>
                    {/* <Item.Description>
                      <p>{movie.release_date}</p>
                      <p>{movie.overview}</p>
                    </Item.Description> */}
                    
                  </Item.Content>
                </Item>
            ))}
          </Item.Group>
{/* 
          <Switch>
            <Route path="/detail/:id" component={Detail}>
            </Route>
          </Switch> */}
        </Container>
    );
  }
}

Search.propTypes = {
  api_key: PropTypes.string
}

export default Search;
