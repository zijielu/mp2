import React from 'react';
import { Header } from "semantic-ui-react"

import './style.scss';

function MyHeader() {
  return (
    <div className="MyHeader">
        <Header as='h1'>Top-rated Popular Movie Directory</Header>
    </div>
  );
}

export default MyHeader;
