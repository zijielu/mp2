import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from "react-router-dom";

import MyHeader from "../MyHeader"
import Search from "../Search"
import Gallery from "../Gallery"
import Detail from "../Detail"

import { Menu, Container } from 'semantic-ui-react'

// import { Container } from "semantic-ui-react";
// import './style.scss';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.apikey = "1c615c4dadd281476f0dfb8a987a6973";
    this.state = { query: '', activeItem: '' };
    this.handleSearchChange = this.handleSearchChange.bind(this);
  }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  handleSearchChange(event) {
    const { value } = event.target;
    this.setState({ query: value });
  }

  render() {
    const { activeItem } = this.state
    return (
      <Router>
        <MyHeader />
        <Container textAlign="center">
          <Menu secondary pointing compact size="huge">
            <Menu.Item 
              as={ Link }
              name="search"
              active={activeItem === 'search'}
              to="/search"
              onClick={this.handleItemClick}>
              Search
            </Menu.Item>
            <Menu.Item 
              as={ Link }
              name="gallery" 
              active={activeItem === 'gallery'}
              to="/gallery" 
              onClick={this.handleItemClick}>
              Gallery
            </Menu.Item>
          </Menu>
        </Container>
      

        {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/search" >
            <Search apikey={this.apikey} />
          </Route>
          <Route path="/gallery">
            <Gallery apikey={this.apikey} />
          </Route>
          <Route exact path="/detail/:id" component={Detail} >  
          </Route>
        </Switch>
      </Router>
    )
  }
}

export default App;
