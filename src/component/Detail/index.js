import React from 'react';
import { withRouter } from "react-router"
import { Grid, Item, Button, Icon, Container } from "semantic-ui-react"
import { Link } from 'react-router-dom'
import './style.scss';
import axios from 'axios';

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.api_key = "1c615c4dadd281476f0dfb8a987a6973";
    // this.id = props.match.params.id;
    this.state = { id: props.match.params.id, movie: [] }
    this.movie = []
    // this.id = props.match.params.id
    console.log("new page with movie id" + parseInt(this.state.id))
    this.api_endpoint = `https://api.themoviedb.org/3/movie/${this.state.id}?api_key=${this.api_key}&language=en-US`
    console.log(this.api_endpoint);
  }
  
  componentDidMount() {
    // this.getData(this.props.match.params.id); 
    axios.get(this.api_endpoint)
    .then(res => {
      console.log(res.data)
      this.setState((state) => ({ movie: res.data }))
      // this.setState((state)) => ({ movie: res.data});
    })
  }

  // componentWillUpdate() {
  //   console.log(this.api_endpoint)
  //   axios.get(this.api_endpoint)
  //   .then(res => {
  //     console.log(res.data)
  //     this.setState((state) => ({ movie: res.data }))
  //   })
  // }

  // changeId = (id) => {
  //   console.log("new received id");
    
    
  // }

  componentDidUpdate(prevProps) {
    if(this.props.match.params.id !== prevProps.match.params.id) {
      console.log("received new params id:" + this.props.match.params.id)
      let id = this.props.match.params.id
      
      this.api_endpoint = `https://api.themoviedb.org/3/movie/${id}?api_key=${this.api_key}&language=en-US`
        console.log("new api endpoint: "+ this.api_endpoint);
         axios.get(this.api_endpoint)
          .then(res => {
            
            // this.movie = res.data;
            this.setState((state) => ({id: id,  movie: res.data }), 
            console.log("setstate movie id to " + parseInt(this.state.movie.id)))
          })
          .catch((error) => {
            this.setState((state) => ({id: id, movie: []}))
          })
    }
  }

  render() {
    
    const { id, movie } = this.state;
    // const movie = this.movie
    // const { id, movie } = this.state;
    console.log("now movieID is " + parseInt(id));  
    const nextId = (parseInt(id) + 1).toString(10);
    const prevId = (parseInt(id) - 1).toString(10);
   
    // console.log("nextId is: " + nextId)
    // console.log("prevId is: " + prevId)   
    console.log(this.state.movie)
    return (
      <Container className="Detail">
        <Grid columns={3} >
          <Grid.Column width={2} textAlign={"center"} verticalAlign={"middle"}>
            <Button icon
              as={Link}
              to={`/detail/${prevId}`}
            >
              <Icon name="left arrow"/>
            </Button>
          </Grid.Column>
            
          <Grid.Column width={12} >
            <Item.Group className="ItemGroup">
              <Item>
                <Item.Image 
                  size="small"
                  className="poster"
                  src={ (movie.length !== 0 && movie.poster_path !== null) ? `https://image.tmdb.org/t/p/w500/${movie.poster_path}` : "https://critics.io/img/movies/poster-placeholder.png"}
                  />
                <Item.Content>
                  <Item.Header as='h2'>{movie !== undefined ? movie.original_title : ""}</Item.Header>
                  <Item.Description>
                    <p>{movie.length !== 0 ? movie.release_date : ""}</p>
                    <p>{movie.length !== 0 ? movie.overview : ""}</p>
                  </Item.Description>
                </Item.Content>
              </Item>
            </Item.Group>
          </Grid.Column>

          <Grid.Column width={2} textAlign="center" verticalAlign={"middle"}>
            <Button icon
              as={Link}
              to={`/detail/${nextId}`}
            >
              <Icon name="right arrow"/>
            </Button>
          </Grid.Column>
        </Grid>
      </Container>
    )
  }
}

export default withRouter(Detail);
