import React from 'react';
import axios from 'axios'
import PropTypes from 'prop-types'

import { Grid, Container, Image, Menu } from "semantic-ui-react"
import { Link } from 'react-router-dom'
import './style.scss';

class Gallery extends React.Component {
  constructor(props) {
    super(props);
    this.state = { movies: [] }; 
    // this.handleItemClick = this.handleItemClick.bind(this);
    this.api_endpoint = `https://api.themoviedb.org/3/movie/550?api_key=${this.props.apikey}`;
    this.genre_hashmap = {};
    this.genre_hashmap['action'] = '28';
    this.genre_hashmap['adventure'] = '12';
    this.genre_hashmap['comedy'] = '35';
    this.genre_hashmap['crime'] = '80';
    this.genre_hashmap['drama'] = '18';
    this.genre_hashmap['horror'] = '27';
    this.genre_hashmap['romance'] = '10749';
    this.genre_hashmap['sci-fi'] = '878';
    this.genre_hashmap['western'] = '37';
  }

  handleItemClick = (e, { name }) => {
    console.log(name)
    this.setState((state, props) => ({ activeItem: name }));
    console.log(this.state.activeItem)
    let discover_api =""
    if (name === "trending") {
      discover_api = `https://api.themoviedb.org/3/discover/movie?api_key=${this.props.apikey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`;
    } else {
      discover_api = `https://api.themoviedb.org/3/discover/movie?api_key=${this.props.apikey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&with_genres=${this.genre_hashmap[name]}`;
    }
    console.log(discover_api);
    axios.get(discover_api)
        .then(res => {
          this.setState({ movies: res.data.results });
        })
    
  }

  componentDidMount() {
    this.setState({ activeItem: "trending" })
    let discover_api = `https://api.themoviedb.org/3/discover/movie?api_key=${this.props.apikey}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`
    console.log(discover_api);
    axios.get(discover_api)
        .then(res => {
          // console.log(res.data.results)
          this.setState({ movies: res.data.results });
        })
  }

  render() {
    const { activeItem } = this.state

    return (
      <Container className="Gallery"> 
        <Container className="Menu-container">
          <Menu secondary stackable widths={10} size="large">
            <Menu.Item
              name="trending"
              active={activeItem === 'trending'}
              onClick={this.handleItemClick}
              >
              Trending
            </Menu.Item>
            <Menu.Item
              name="action"
              active={activeItem === 'action'}
              onClick={this.handleItemClick}
              >
            Action
            </Menu.Item>
            <Menu.Item
              name="adventure"
              active={activeItem === 'adventure'}
              onClick={this.handleItemClick}
              >
            Adventure
            </Menu.Item>
            <Menu.Item
              name="comedy"
              active={activeItem === 'comedy'}
              onClick={this.handleItemClick}
              >
              Comedy
            </Menu.Item>
            <Menu.Item
              name="crime"
              active={activeItem === 'crime'}
              onClick={this.handleItemClick}
              >
              Crime
            </Menu.Item>
            <Menu.Item
              name="drama"
              active={activeItem === 'drama'}
              onClick={this.handleItemClick}
              >
            Drama
            </Menu.Item>
            <Menu.Item
              name="horror"
              active={activeItem === 'horror'}
              onClick={this.handleItemClick}
              >
            Horror
            </Menu.Item>
            <Menu.Item
              name="romance"
              active={activeItem === 'romance'}
              onClick={this.handleItemClick}
              >
            Romance
            </Menu.Item>
            <Menu.Item
              name="sci-fi"
              active={activeItem === 'sci-fi'}
              onClick={this.handleItemClick}
              >
            Sci-fi
            </Menu.Item>
            <Menu.Item
              name="western"
              active={activeItem === 'western'}
              onClick={this.handleItemClick}
              >
            Western
            </Menu.Item>
          </Menu>
        </Container>
        <Grid centered columns={4}>
          {this.state.movies.map((movie) => (
            <Grid.Column key={movie.id}>
              <Image 
                className="poster"  
                src={`https://image.tmdb.org/t/p/w500/${movie.poster_path}`} 
                as={Link}
                to={`/detail/${movie.id}`}
              />
            </Grid.Column>
            // <Container className="Gallery-item" key={movie.id}>
            // </Container>
          ))}
        </Grid>
      </Container>
    )
  }
}

Gallery.propTypes = {
  api_key: PropTypes.string
}

export default Gallery;
